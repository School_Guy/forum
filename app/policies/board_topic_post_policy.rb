class BoardTopicPostPolicy < ApplicationPolicy
  def new?
    create?
  end

  def create?
    @user
  end

  def comment?
    create?
  end

  def delete?
    @user && (@user.admin || @user.moderator || (@user == @record.user))
  end

  def delete_file?
    delete? && @record.file.attached?
  end
end
