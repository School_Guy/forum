class UserPolicy < ApplicationPolicy
  def logout?
    # Pundit: True/Objekt (Objekt ist true) --> weitere Verarbeitung
    # Pundit: False --> 403
    @user
  end

  def login?
    @user.nil?
  end

  def login_perform?
    login?
  end
end
