class BoardTopicsController < ApplicationController
  # GET /boards/:id/topics/new
  def new
    authorize BoardTopic
    Board.find(params[:id])
    @board_topic = BoardTopic.new
    @board_topic_post = BoardTopicPost.new
  end

  # POST /boards/:id/topics/create
  def create
    authorize BoardTopic
    user = User.current
    board = Board.find(params[:id])
    ApplicationRecord.transaction do
      @board_topic = BoardTopic.new(
          board: board,
          title: params[:board_topic][:title],
          username: user.username,
          user: user
      )
      @board_topic_post = BoardTopicPost.new(
          username: user.username,
          content: params[:board_topic][:board_topic_post][:content],
          user: user
      )
      if @board_topic.save
        @board_topic_post.board_topic = @board_topic
        if @board_topic_post.save
          notify_on_post_update @board_topic_post
          @board_topic = BoardTopic.new
          @board_topic_post = BoardTopicPost.new
          flash.now[:ok] = 'saved'
        else
          flash.now[:error] = @board_topic_post.errors.full_messages.to_sentence
          raise ActiveRecord::Rollback
        end
      else
        flash[:error] = @board_topic.errors.full_messages.to_sentence
      end
    end
    render :new
  end

  # GET /boards/:board_id/:id/
  def show
    @board_topic = BoardTopic.find(params[:id])
    @board_topic_post_answer = BoardTopicPost.new
    if @board_topic.to_param != params[:id] || @board_topic.board.to_param != params[:board_id]
      redirect_to board_topics_show_path(@board_topic.board, @board_topic)
      return
    end
    @posts = @board_topic.board_topic_posts
  end

  private

  def notify_on_post_update(post)
    BoardTopicNotificationsChannel.broadcast_to(
        post.board_topic,
        { event: 'post_updated', query_path: board_topic_posts_list_path(post.board_topic)}
    )
  end
end
