class UsersController < ApplicationController
  # GET /user/new
  def new
    @user = User.new
  end

  # POST /user/new
  def create
    @user = User.new(
        username: params[:user][:username],
        email: params[:user][:email],
        password: params[:user][:password],
        password_confirmation: params[:user][:password_confirmation]
    )
    if @user.save
      set_login_cookie @user
      @user = User.new
      flash.now[:success] = 'Successfully saved!'
    else
      flash.now[:error] = @user.errors.full_messages.to_sentence
    end
    render :new
  end

  # POST /user/logout
  def logout
    authorize User
    cookies.delete :forum_account
    redirect_to root_path
  end

  # GET /user/login
  def login
    authorize User
    @user = User.new
  end

  # POST /user/login
  def login_perform
    authorize User
    @user = User.find_by username: params[:user][:username]
    if @user.try :authenticate, params[:user][:password]
      set_login_cookie @user
      User.current = @user
      flash.now[:success] = 'Login successful!'
    else
      flash.now[:error] = 'Login unsuccessful!'
    end
    render :login
  end

  private

  def set_login_cookie(user)
    cookies.encrypted.permanent[:forum_account] = {
        value: JSON.generate({
                                 user_id: user.id,
                                 lifetime: 7.days.from_now
                             }),
        secure: Rails.env.production?
    }
  end
end
