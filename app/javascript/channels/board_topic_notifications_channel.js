import consumer from "./consumer"

document.addEventListener("turbolinks:load", function() {
    $(document).ready( function () {
        consumer.subscriptions.create({channel: "BoardTopicNotificationsChannel", id: $('#board-topic-id').data('id')}, {
            connected() {
                // Called when the subscription is ready for use on the server
                console.debug("connected");
                console.debug($('#board-topic-id').data('id'))
            },

            disconnected() {
                // Called when the subscription has been terminated by the server
                console.debug("disconnected")
            },

            received(data) {
                // Called when there's incoming data on the websocket for this channel
                console.debug("data received");
                console.debug(data);
                if (data['event'] === 'post_updated') {
                    $.get(data['query_path'], null, function (response) {
                        $('#posts-list').html(response)
                    });
                }
            }
        });
    });
});