class CreateBoardTopics < ActiveRecord::Migration[6.0]
  def change
    create_table :board_topics do |t|
      t.string :title
      t.belongs_to :board
      t.string :username
      t.timestamps
    end
  end
end
